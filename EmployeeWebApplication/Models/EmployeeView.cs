﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeWebApplication.Models
{
    public class EmployeeView
    {
        public int EmployeeId { get; set; }

        public string EmployeeName { get; set; }

        public string DepartmentName { get; set; }

        public int DepartmentId { get; set; }

        public DateTime JoinedDate { get; set; }

        public string PhotoFileName { get; set; }
    }
}
