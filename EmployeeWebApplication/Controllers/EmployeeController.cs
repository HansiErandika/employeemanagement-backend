﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmployeeWebApplication.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Net.Http.Headers;

namespace EmployeeWebApplication.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly EmployeeContext _context;
        private readonly IHostingEnvironment _env;

        private static string dBPath;

        public EmployeeController(EmployeeContext context, IHostingEnvironment env)
        {
            _context = context;
            _env = env;
        }

        [HttpGet]
        public IEnumerable<EmployeeView> Get()
        {
            List<Employee> employees = _context.Employees.ToList();

            List<EmployeeView> empview = new List<EmployeeView>();

            foreach(var emp in employees)
            {
                Department department = _context.Departments.Find(emp.DepartmentId);
                //Select(c=> c.DepartmentId==emp.DepartmentId);

                empview.Add(new EmployeeView() { EmployeeId = emp.EmployeeId, EmployeeName = emp.EmployeeName, DepartmentName= department.DepartmentName, DepartmentId = department.DepartmentId, JoinedDate= emp.JoinedDate, PhotoFileName=emp.PhotoFileName });
            }

            return empview;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetEmployee([FromRoute] int id)
        {
            var emp = await _context.Employees.FindAsync(id);
            Department department = _context.Departments.Find(emp.DepartmentId);
            EmployeeView employee=new EmployeeView() { EmployeeId = emp.EmployeeId, EmployeeName = emp.EmployeeName, DepartmentName = department.DepartmentName, DepartmentId=department.DepartmentId, JoinedDate = emp.JoinedDate, PhotoFileName = emp.PhotoFileName };

            if (employee == null)
            {
                return NotFound(); ;
            }
            return Ok(employee);
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateEmployee([FromRoute] int id, [FromBody] Employee Employee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != Employee.EmployeeId)
            {
                return BadRequest();
            }

            _context.Entry(Employee).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EmployeesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        private bool EmployeesExists(int id)
        {
            return _context.Employees.Any(e => e.EmployeeId == id);
        }

        [HttpPost]
        public async Task<IActionResult> AddEmployee([FromBody] Employee employee)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.Employees.Add(employee);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Get", new { id = employee.EmployeeId }, employee);
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEmployee([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var employee = await _context.Employees.FindAsync(id);
            if (employee == null)
            {
                return NotFound();
            }

            _context.Employees.Remove(employee);
            await _context.SaveChangesAsync();

            return Ok(employee);
        }

        [Route("SaveFile")]
        [HttpPost]
        public JsonResult SaveFile()
        {
            try
            {
                var httpRequest = Request.Form;
                var postedFile = httpRequest.Files[0];
                //string filename = postedFile.FileName;
                var pathToSave = Path.Combine(Directory.GetCurrentDirectory(), "Photos");
                //var physicalPath = _env.ContentRootPath + "/Photos/" + filename;
                if (postedFile.Length > 0)
                {
                    var fileName = ContentDispositionHeaderValue.Parse(postedFile.ContentDisposition).FileName.Trim('"');
                    var fullPath = Path.Combine(pathToSave, fileName);
                    dBPath = Path.Combine("Photos", fileName);

                    using (var stream = new FileStream(fullPath, FileMode.Create))
                    {
                        postedFile.CopyTo(stream);
                    }

                    return new JsonResult(fileName);
                }
                else
                {
                    return new JsonResult(BadRequest());
                }
                

                //using(var stream=new FileStream(physicalPath, FileMode.Create))
                //{
                //    postedFile.CopyTo(stream);
                //}

                //return new JsonResult(filename);

            }
            catch (Exception)
            {
                return new JsonResult("anonymus.png");
            }
        }
    }
}